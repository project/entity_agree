<?php

/**
 * @file
 * Entity agreement 'default' agree_form plugin.
 */

$plugin = [
  'type' => 'user_form_widget',
  'label' => t('Checkbox on user form'),
  'class' => 'EntityAgreeUserFormCheckboxPlugin',
];
