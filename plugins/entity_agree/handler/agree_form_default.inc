<?php

/**
 * @file
 * Entity agreement 'default' agree_form plugin.
 */

$plugin = [
  'type' => 'agree_form',
  'label' => t('Default form'),
  'class' => 'EntityAgreeAgreeFormDefaultPlugin',
];
