<?php

/**
 * @file
 * Entity agreement 'default' notification_block plugin.
 */

$plugin = [
  'type' => 'notification_block',
  'label' => t('Default'),
  'class' => 'EntityAgreeNotificationBlockDefault',
];
