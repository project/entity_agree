<?php

/**
 * @file
 * Entity agreement 'role' apply plugin.
 */

$plugin = [
  'type' => 'apply',
  'label' => t('All users'),
  'class' => 'EntityAgreeApplyAllPlugin',
];
