<?php

/**
 * @file
 * Entity agreement 'role' apply plugin.
 */

$plugin = [
  'type' => 'apply',
  'label' => t('By role'),
  'class' => 'EntityAgreeApplyRolePlugin',
];
