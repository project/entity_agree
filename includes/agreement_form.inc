<?php

/**
 * @file
 * Entity Agree agreement form.
 */

/**
 * Form for agreeing.
 */
function entity_agree_agreement_form($form, &$form_state, $entity_type, stdClass $entity, array $config) {
  $form['#entity_type'] = $entity_type;
  $form['#entity'] = $entity;

  $button_text = !empty($config["button_text"]) ? $config["button_text"] : t('Agree');

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => $button_text,
  ];

  return $form;
}

/**
 * Submit handler for entity_agree_agreement_form.
 */
function entity_agree_agreement_form_submit($form, &$form_state) {
  list($id, $vid) = entity_extract_ids($form['#entity_type'], $form['#entity']);

  /** @var \EntityAgree $agreement */
  $agreement = entity_create('entity_agree', [
    'entity_type' => $form['#entity_type'],
    'entity_id' => $id,
    'entity_vid' => $vid,
  ]);
  $agreement->save();

  // Confirmation message.
  if (!empty($form['#field_item']['confirm'])) {
    $message = check_markup($form['#field_item']['confirm'], $form['#field_item']['confirm_format']);
  }
  else {
    $message = t('You have agreed.');
  }
  drupal_set_message($message);
}
