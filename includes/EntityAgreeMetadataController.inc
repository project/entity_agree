<?php

/**
 * Class EntityAgreeMetadataController.
 */
class EntityAgreeMetadataController extends EntityDefaultMetadataController {

  /**
   * {@inheritdoc}
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['id']['label'] = t('ID');
    $properties['id']['description'] = t('The agreement entity ID.');

    $properties['entity_type']['label'] = t('Entity Type');
    $properties['entity_type']['description'] = t('Entity type of the entity agreed to.');

    $properties['entity_id']['label'] = t('Entity ID');
    $properties['entity_id']['description'] = t('Entity ID of the entity agreed to.');

    $properties['entity'] = [
      'label' => t('Entity'),
      'type' => 'entity',
      'description' => t('The entity agreed to.'),
      'getter callback' => 'entity_agree_entity_getter',
      'setter callback' => 'entity_agree_entity_setter',
      'computed' => TRUE,
    ];

    $properties['uid']['label'] = t('User');
    $properties['uid']['description'] = t('The user who agreed.');
    $properties['uid']['type'] = 'user';

    $properties['timestamp']['label'] = t('Date/time');
    $properties['timestamp']['description'] = t('The date and time the user agreed.');
    $properties['timestamp']['type'] = 'date';

    $properties['ip_address']['label'] = t('IP address');
    $properties['ip_address']['description'] = t('IP address of the user who agreed.');

    return $info;
  }

}
