<?php

/**
 * @file
 * Entity Agree blocks code.
 */

/**
 * Implements hook_block_info().
 */
function entity_agree_block_info() {
  $blocks['needs_agreed'] = [
    'info' => t('Agreements needed'),
    'cache' => DRUPAL_NO_CACHE,
  ];

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function entity_agree_block_view() {
  $content = [];

  $needed = entity_agree_find_needed_agreements();
  foreach ($needed as $entity_type => $entities) {
    foreach ($entities as $entity) {
      if ($value = entity_agree_get_entity_agree_config($entity_type, $entity)) {
        $plugins = entity_agree_get_plugin_config_by_type($value['config'], 'notification_block');
        if ($plugin = reset($plugins)) {
          /** @var \EntityAgreeNotificationBlockInterface $plugin */
          $plugin = entity_agree_instantiate_plugin($plugin['type'], $plugin['config']);
          $new = entity_agree_user_agree_entity($entity_type, $entity) ? FALSE : TRUE;
          $content[] = $plugin->getContent($new);
        }
      }
    }
  }

  if (!$content) {
    return;
  }

  $block['subject'] = t('Agreements needed');
  $block['content'] = array(
    '#theme' => 'item_list',
    '#items' => $content,
  );

  return $block;
}
