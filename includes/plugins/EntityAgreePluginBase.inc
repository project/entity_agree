<?php

/**
 * Base class for plugins.
 */
abstract class EntityAgreePluginBase implements EntityAgreePluginInterface {

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected $config = [];

  /**
   * The plugin definition.
   *
   * @var array
   */
  protected $pluginDefinition = [];

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $config) {
    $this->config = $config;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginDefinition(array $pluginDefinition) {
    $this->pluginDefinition = $pluginDefinition;
    return $this;
  }

  /**
   * Obtains the current user.
   *
   * @return \stdClass
   *   The current user.
   */
  public function currentUser() {
    global $user;
    return user_load($user->uid);
  }

}
