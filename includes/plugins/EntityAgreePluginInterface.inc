<?php

/**
 * Interface EntityAgreePluginInterface.
 */
interface EntityAgreePluginInterface {

  /**
   * Generates the plugin configuration form.
   *
   * @param array $element
   *   The element being attached to.
   */
  public function configForm(array $element);

  /**
   * Sets the plugin configuration.
   *
   * @param array $config
   *   The plugin configuration.
   *
   * @return $this
   */
  public function setConfiguration(array $config);

  /**
   * Sets the plugin definition.
   *
   * @param array $pluginDefinition
   *   The plugin definition.
   *
   * @return $this
   */
  public function setPluginDefinition(array $pluginDefinition);

  /**
   * Provides the plugin label.
   *
   * @return string
   *   The plugin label.
   */
  public function label();

}
