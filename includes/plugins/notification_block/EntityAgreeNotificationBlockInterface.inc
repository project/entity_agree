<?php

interface EntityAgreeNotificationBlockInterface extends EntityAgreePluginInterface {

  /**
   * Gets the content of the block.
   *
   * @param bool $new
   *   Indicates if the agreement is new (user has not yet accepted). If not new
   *   then it is assumed that the user has agreed at some point and the
   *   agreement has later been updated.
   *
   * @return string|array
   *   A string or renderable array.
   */
  public function getContent($new = TRUE);

}
