<?php

class EntityAgreeNotificationBlockDefault extends EntityAgreePluginBase implements EntityAgreeNotificationBlockInterface {

  /**
   * {@inheritdoc}
   */
  public function getContent($new = TRUE) {
    if ($new) {
      return check_markup($this->config['new']['value'], $this->config['new']['format']);
    }
    return check_markup($this->config['updated']['value'], $this->config['updated']['format']);
  }

  /**
   * {@inheritdoc}
   */
  public function configForm(array $element) {
    return [
      'new' => [
        '#type' => 'text_format',
        '#title' => t('New'),
        '#description' => t('Message when the user has not yet accepted the agreement.'),
        '#default_value' => !empty($this->config['new']['value']) ? $this->config['new']['value'] : NULL,
        '#format' => !empty($this->config['new']['format']) ? $this->config['new']['format'] : NULL,
      ],
      'updated' => [
        '#type' => 'text_format',
        '#title' => t('Updated'),
        '#description' => t('Message to the user when the agreement has been updated.'),
        '#default_value' => !empty($this->config['updated']['value']) ? $this->config['updated']['value'] : NULL,
        '#format' => !empty($this->config['updated']['format']) ? $this->config['updated']['format'] : NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return t('Notification block');
  }

}
