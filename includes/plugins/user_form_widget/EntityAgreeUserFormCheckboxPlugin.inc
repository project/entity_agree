<?php

/**
 * Class EntityAgreeUserFormCheckboxPlugin.
 */
class EntityAgreeUserFormCheckboxPlugin extends EntityAgreePluginBase implements EntityAgreeUserFormWidgetInterface {

  /**
   * {@inheritdoc}
   */
  public function configForm(array $form) {
    return [
      'label' => [
        '#type' => 'textfield',
        '#title' => t('Checkbox label'),
        '#default_value' => !empty($this->config['label']) ? $this->config['label'] : NULL,
        '#format' => !empty($this->config['label']) ? $this->config['label'] : NULL,
      ],
      'description' => [
        '#type' => 'text_format',
        '#title' => t('Description'),
        '#default_value' => !empty($this->config['description']['value']) ? $this->config['description']['value'] : NULL,
        '#format' => !empty($this->config['description']['format']) ? $this->config['description']['format'] : NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return t('Checkbox');
  }

  /**
   * {@inheritdoc}
   */
  public function widgetForm(array $form, array &$form_state, $entity_type, stdClass $entity) {
    list($id) = entity_extract_ids($entity_type, $entity);
    $build = [
      '#title' => $this->config['label'],
      '#description' => check_markup($this->config['description']['value'], $this->config['description']['format']),
      '#type' => 'checkbox',
      '#element_validate' => ['_entity_agree_user_form_checkbox_validate'],
      '#current_user' => TRUE,
      '#entity' => $entity,
      '#entity_type' => $entity_type,
      '#entity_id' => $id,
    ];

    // If currently agreed.
    if (entity_agree_user_agreed_current($entity_type, $entity, $form['#user'])) {
      $build['#entity_agree_current'] = TRUE;
      $build['#default_value'] = TRUE;
      $build['#disabled'] = TRUE;
    }
    else {
      $build['#entity_agree_current'] = FALSE;
      $build['#default_value'] = FALSE;
    }

    // Handle other users' checkboxes.
    if ($this->currentUser()->uid != $form['#user']->uid) {
      $build['#disabled'] = TRUE;
      $build['#current_user'] = FALSE;
    }
    elseif (!user_access('bypass entity agreements')) {
      $build['#required'] = TRUE;
    }

    return $build;
  }

}
