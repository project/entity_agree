<?php

/**
 * Class EntityAgreeUserFormWidgetInterface.
 */
interface EntityAgreeUserFormWidgetInterface extends EntityAgreePluginInterface {

  /**
   * Creates the form widget for the user profile.
   *
   * @param array $form
   *   The form array.
   * @param array $form_state
   *   The form state array.
   * @param string $entity_type
   *   The agreement entity type.
   * @param \stdClass $entity
   *   The agreement entity.
   *
   * @return mixed
   *   The profile form widget.
   */
  public function widgetForm(array $form, array &$form_state, $entity_type, stdClass $entity);

}
