<?php

/**
 * Provides the 'all' apply plugin.
 */
class EntityAgreeApplyAllPlugin extends EntityAgreePluginBase implements EntityAgreeApplyPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return t('All');
  }

  /**
   * {@inheritdoc}
   */
  public function configForm(array $element) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function applies(stdClass $account = NULL) {
    // Always applies.
    return TRUE;
  }

}
