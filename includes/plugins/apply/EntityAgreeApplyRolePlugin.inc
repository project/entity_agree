<?php

/**
 * Provides the 'role' apply plugin.
 */
class EntityAgreeApplyRolePlugin extends EntityAgreePluginBase implements EntityAgreeApplyPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return t('Role');
  }

  /**
   * {@inheritdoc}
   */
  public function configForm(array $element) {
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => t('Roles'),
      '#options' => user_roles(TRUE),
      '#default_value' => !empty($this->config['roles']) ? $this->config['roles'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(stdClass $account = NULL) {
    if (!$account) {
      $account = $this->currentUser();
    }

    $matching_roles = array_intersect(array_keys($account->roles), array_filter($this->config['roles']));
    return !empty($matching_roles);
  }

}
