<?php

/**
 * Interface EntityAgreeApplyPluginInterface.
 */
interface EntityAgreeApplyPluginInterface extends EntityAgreePluginInterface {

  /**
   * Determines if the agreement applies to the user.
   *
   * @param \stdClass $account
   *   The user account. Optional, defaults to the current user.
   *
   * @return bool
   *   Indicates if the agreement applies to the user.
   */
  public function applies(stdClass $account = NULL);

}
