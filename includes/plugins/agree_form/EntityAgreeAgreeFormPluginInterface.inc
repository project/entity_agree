<?php

interface EntityAgreeAgreeFormPluginInterface {

  /**
   * Gets the agreement form.
   *
   * @param string $entity_type
   * @param \stdClass $entity
   *
   * @return array
   *   The form array.
   */
  public function getForm($entity_type, $entity);

}
