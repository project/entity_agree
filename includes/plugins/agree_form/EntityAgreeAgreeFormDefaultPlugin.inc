<?php

/**
 * Class EntityAgreeAgreeFormDefaultPlugin.
 */
class EntityAgreeAgreeFormDefaultPlugin extends EntityAgreePluginBase implements EntityAgreeAgreeFormPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function configForm(array $form) {
    return [
      'button_text' => [
        '#type' => 'textfield',
        '#title' => t('Button text'),
        '#maxlength' => 32,
        '#size' => 20,
        '#default_value' => !empty($this->config['button_text']) ? $this->config['button_text'] : t('Agree'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return t('Default');
  }

  /**
   * {@inheritdoc}
   */
  public function getForm($entity_type, $entity) {
    return drupal_get_form('entity_agree_agreement_form', $entity_type, $entity, $this->config);
  }

}
