<?php

/**
 * Class EntityAgree.
 */
class EntityAgree extends Entity {

  /**
   * {@inheritdoc}
   */
  public function save() {
    global $user;

    if (!isset($this->uid)) {
      $this->uid = $user->uid;
    }

    if (!isset($this->timestamp)) {
      $this->timestamp = $this->getCurrentTimestamp();
    }

    if (!isset($this->ip_address)) {
      $this->ip_address = $this->getIpAddress();
    }

    parent::save();

    cache_clear_all('user:' . $this->uid . ':', 'cache_entity_agree', TRUE);
    _entity_agree_static_reset();
  }

  /**
   * Gets the user's IP address.
   *
   * @return string
   *   The IP address as given by Drupal.
   */
  protected function getIpAddress() {
    return ip_address();
  }

  /**
   * Gets the current timestamp.
   *
   * @return string
   *   The current timestamp as given by Drupal.
   */
  protected function getCurrentTimestamp() {
    return REQUEST_TIME;
  }

}
