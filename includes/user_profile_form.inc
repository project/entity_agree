<?php

/**
 * @file
 * Entity Agreements user profile form functionality.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function entity_agree_form_user_profile_form_alter(&$form, &$form_state) {
  // Only show up on the standard user form, not additional profile/categories.
  if ($form['#user_category'] != 'account') {
    return;
  }

  $agreements = entity_agree_find_needed_agreements($form['#user'], TRUE);

  // Add in the agreements.
  foreach ($agreements as $entity_type => $entities) {
    foreach ($entities as $entity_id => $entity) {
      if ($value = entity_agree_get_entity_agree_config($entity_type, $entity)) {
        $plugins = entity_agree_get_plugin_config_by_type($value['config'], 'user_form_widget');
        if ($plugin = reset($plugins)) {
          /** @var \EntityAgreeUserFormWidgetInterface $plugin */
          $plugin = entity_agree_instantiate_plugin($plugin['type'], $plugin['config']);
          $build = $plugin->widgetForm($form, $form_state, $entity_type, $entity);
          $form['entity_agree']["$entity_type:$entity_id"] = $build;
        }
      }
    }
  }

  // If there are agreements, add in the form element. Otherwise leave empty.
  if (!empty($form['entity_agree'])) {
    $form['entity_agree']['#type'] = 'fieldset';
    $form['entity_agree']['#title'] = t('Agreements');
    $form['entity_agree']['#tree'] = TRUE;
    $form['#submit'][] = '_entity_agree_user_profile_agree_submit';
  }

  // Allow other modules to alter what we've done here.
  drupal_alter('entity_agree_form_user_profile', $form, $form_state);
}

/**
 * Validates agreement checkboxes on user profile form.
 *
 * Set the entity into storage for saving via form submit handler. This allows
 * the form to be modified without breaking our handling of agreements to save.
 */
function _entity_agree_user_form_checkbox_validate(&$element, &$form_state) {
  // Default to not agreeing.
  $form_state['storage']['entity_agree'][$element['#entity_type']][$element['#entity_id']] = FALSE;

  if (empty($element['#current_user'])) {
    return;
  }
  if (!empty($element['#entity_agree_current'])) {
    return;
  }
  if (empty($element['#value'])) {
    return;
  }

  $form_state['storage']['entity_agree'][$element['#entity_type']][$element['#entity_id']] = $element['#entity'];
}

/**
 * Submit handler to agree to the agreements.
 */
function _entity_agree_user_profile_agree_submit($form, &$form_state) {
  if (empty($form_state['storage']['entity_agree'])) {
    return;
  }

  foreach ($form_state['storage']['entity_agree'] as $entity_type => $entities) {
    $entities = array_filter($entities);
    foreach ($entities as $entity) {
      list($id, $vid) = entity_extract_ids($entity_type, $entity);
      $agreement = entity_create('entity_agree', [
        'entity_type' => $entity_type,
        'entity_id' => $id,
        'entity_vid' => $vid,
        'uid' => $form['#user']->uid,
      ]);
      $agreement->save();
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function entity_agree_form_user_register_form_alter(&$form, &$form_state) {
  // @TODO: Revisit this with anonymous user handling, convert to user.
  // Fake an already-created, authenticated user.
  $account = new stdClass();
  $account->uid = -1;
  $account->roles = [2 => 2];

  $agreements = entity_agree_find_needed_agreements($account, TRUE);

  // Add in the agreements.
  foreach ($agreements as $entity_type => $entities) {
    foreach ($entities as $entity_id => $entity) {
      if ($value = entity_agree_get_entity_agree_config($entity_type, $entity)) {
        $plugins = entity_agree_get_plugin_config_by_type($value['config'], 'user_form_widget');
        if ($plugin = reset($plugins)) {
          /** @var \EntityAgreeUserFormWidgetInterface $plugin */
          $plugin = entity_agree_instantiate_plugin($plugin['type'], $plugin['config']);
          $build = $plugin->widgetForm($form, $form_state, $entity_type, $entity);
          $form['entity_agree']["$entity_type:$entity_id"] = $build;
        }
      }
    }
  }

  // If there are agreements, add in the form element. Otherwise leave empty.
  if (!empty($form['entity_agree'])) {
    $form['entity_agree']['#type'] = 'fieldset';
    $form['entity_agree']['#title'] = t('Agreements');
    $form['entity_agree']['#tree'] = TRUE;
    $form['#submit'][] = '_entity_agree_user_profile_agree_submit';
  }

  // Allow other modules to alter what we've done here.
  drupal_alter('entity_agree_form_user_register', $form, $form_state);
}
