<?php

/**
 * Class EntityAgreeViewsController.
 */
class EntityAgreeViewsController extends EntityDefaultViewsController {

  /**
   * {@inheritdoc}
   */
  public function views_data() {
    $data = parent::views_data();

    $data['entity_agree']['entity'] = [
      'title' => t('Entity'),
      'help' => t('The entity agreed to.'),
      'field' => [
        'real field' => 'id',
        'handler' => 'entity_agree_views_handler_field_entity',
      ],
    ];

    return $data;
  }

}
