<?php

/**
 * Views handler for entity agreed to.
 */
class entity_agree_views_handler_field_entity extends views_handler_field_entity {

  /**
   * {@inheritdoc}
   */
  public function pre_render(&$values) {
    parent::pre_render($values);

    // Preload all of the referenced entities.
    $ids = [];
    foreach ($this->entities as $entity) {
      $ids[$entity->entity_type][] = $entity->entity_id;
    }
    foreach ($ids as $entity_type => $entity_ids) {
      entity_load($entity_type, $entity_ids);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $value = $this->get_value($values);
    $w = entity_metadata_wrapper('entity_agree', $value);
    $entity = $w->entity->value();
    $uri = entity_uri($value->entity_type, $entity);
    return l(entity_label($value->entity_type, $entity), $uri['path'], $uri['options']);
  }

}
